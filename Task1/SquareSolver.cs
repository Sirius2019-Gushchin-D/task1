using System;
using System.Globalization;

namespace Task1
{
    public static class SquareSolver
    {
        private const string Empty     = "∅";
        private const string Rational  = "ℝ";
        private const string NotIn     = "∉";
        private const string In        = "∈";
        private const string SqRoot    = "√";
        private const string PlusMinus = "±";
        private const string Lower1    = "₁";
        private const string Lower2    = "₂";
        private const string Upper2    = "²";

        public static string GetEquation(double a, double b, double c)
        {
            return a == 0d ? b == 0d ? $"{c} = 0" :
                             c == 0d ? $"{(b == 1d ? "" : $"{b}")}x = 0" :
                                       $"{b}x {Sgn(c)} = 0" :
                   b == 0d ? c == 0d ? $"{a}x{Upper2} = 0" :
                                       $"{a}x{Upper2} {Sgn(c)} = 0" :
                   c == 0d ? $"{a}x{Upper2} {Sgn(b)}x = 0" :
                             $"{a}x{Upper2} {Sgn(b)}x {Sgn(c)} = 0";
        }

        public static (string simple, string solved) GetSolutions(double a, double b, double c)
        {
            var any  = ("x - любое число", $"x {In} {Rational}");
            var none = ("Нет решений", $"x {In} {Empty} (x {NotIn} {Rational})");
            if (a == 0d)
            {
                if (b == 0d) return c == 0d ? any : none;
                var sol = -c / b;
                return ($"{(IsInv(sol) ? $"x = {-c}/{b}" : $"x = {sol}")}", $"x = {sol}");
            }

            if (b == 0d)
            {
                if (c == 0d) return ("x = 0", "x = 0");
                var hsol = -c / a;
                if (hsol < 0) return none;
                if (IsInv(Math.Sqrt(hsol)))
                    return IsInv(hsol)
                               ? ($"x = {PlusMinus}{SqRoot}({-c}/{a})", $"x = {PlusMinus}{Math.Sqrt(hsol)}")
                               : ($"x = {PlusMinus}{SqRoot}({hsol})", $"x = {PlusMinus}{Math.Sqrt(hsol)}");
                return ($"x = {PlusMinus}{Math.Sqrt(hsol)}", $"x = {PlusMinus}{Math.Sqrt(hsol)}");
            }

            if (c == 0d)
            {
                var sol = -b / a;
                return ($"x{Lower1} = 0; x{Lower2} = {(IsInv(sol) ? $"{-b}/{a}" : $"{sol}")}",
                        $"x{Lower1} = 0; x{Lower2} = {sol}");
            }

            var d = b * b - 4 * a * c;
            if (d < 0d) return none;
            if (d == 0d)
                return ($"x = {(IsInv(-b / (2 * a)) ? $"{-b}/{2 * a}" : $"{-b / (2 * a)}")}", $"x = {-b / (2 * a)}");
            var sqrD = Math.Sqrt(d);
            var sol1 = (-b + sqrD) / (2 * a);
            var sol2 = (-b - sqrD) / (2 * a);
            return (
                       $"x{Lower1} = {(IsInv(sol1) ? $"{(IsInv(sqrD) ? $"({-b} + {SqRoot}({d}))/{2 * a}" : $"{-b + sqrD}/{2 * a}")}" : $"{sol1}")}; " +
                       $"x{Lower2} = {(IsInv(sol2) ? $"{(IsInv(sqrD) ? $"({-b} - {SqRoot}({d}))/{2 * a}" : $"{-b - sqrD}/{2 * a}")}" : $"{sol2}")}"
                      ,
                       $"x{Lower1} = {sol1}; x{Lower2} = {sol2}"
                   );
        }

        private static string Sgn(double n) => n >= 0 ? $"+ {n}" : $"- {Math.Abs(n)}";

        private static bool IsInv(double num)
        {
            var st = num.ToString(CultureInfo.CurrentCulture).Split(',');
            if (st.Length       == 1) return false;
            return st[1].Length > 5;
        }
    }
}