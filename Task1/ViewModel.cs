using System.ComponentModel;

namespace Task1
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private double _a;

        public double A
        {
            get => _a;
            set
            {
                _a = value;
                OnPropertyChanged("Solution1");
                OnPropertyChanged("Solution2");
                OnPropertyChanged("Equation");
            }
        }

        private double _b;

        public double B
        {
            get => _b;
            set
            {
                _b = value;
                OnPropertyChanged("Solution1");
                OnPropertyChanged("Solution2");
                OnPropertyChanged("Equation");
            }
        }

        private double _c;

        public double C
        {
            get => _c;
            set
            {
                _c = value;
                OnPropertyChanged("Solution1");
                OnPropertyChanged("Solution2");
                OnPropertyChanged("Equation");
            }
        }

        public string Solution1 => SquareSolver.GetSolutions(A, B, C).simple;
        public string Solution2 => SquareSolver.GetSolutions(A, B, C).solved;
        public string Equation  => SquareSolver.GetEquation(A, B, C);
    }
}